import { html, css, LitElement } from 'lit';
import { ActualUbicationUseCase } from '../usecases/actual-ubication.usecase';
import { ObtainRecipeUseCase } from '../usecases/obtain-recipe.usecase';
import { ShowRecipeUseCase } from '../usecases/show-recipe.usecase';
import axios from 'axios';

class HomePage extends LitElement {
  static styles = css`
  
  `;

  static get properties(){
    return {
      recipes:{ type : Array },
      temperature:{ type: Number },
    }
  }

  constructor(){
    super();
    this.temperature = null;
    this.recipes = [];
    this.latitude = null;
    this.longitude = null;
  }
    
  async connectedCallback() {
    super.connectedCallback();
    this.recommendedRecipes(this.temperature);
  }

  render(){
    let message = '';
    if(this.temperature < 10){
      message ="En el lugar en el que estás hace frío, te recomiendo: ";
    }else if(this.temperature < 20){
      message ="En el lugar en el que estás hace fresco, te recomiendo: "; 
    }else{
      message ="En el lugar en el que estás hace calor, te recomiendo: ";
      
    }
    return html`
    <h1 class="menu-title">Bienvenid@ a FoodApp</h1>
    ${this.temperature !== null && html`<p class="temperature">Temperatura actual: ${this.temperature}°C</p>`}
    ${this.recipes.length > 0 && html`
      <h2 class="message">${message}</h2>
      <div class="recipes-container">
        ${this.recipes.map(
          recipe => html`
            <div class="recipe-card">
              <h3 class="recipe-title">${recipe.title}</h3>
              <img
                class="recipe-image"
                src="${recipe.image}"
                alt="${recipe.title}"
                @click="${() => this.showRecipeDetails(recipe)}"
                />
              ${this.selectedRecipe && this.selectedRecipe.id === recipe.id
                ? html`
                    <div class="recipe-details">
                      <h4>Steps:</h4>
                      <ol>
                        ${this.selectedRecipe.steps.map(
                          step => html`
                            <li>${step}</li>
                          `
                        )}
                      </ol>
                    </div>
                  `
                : ''}
            </div>
          `
        )}
    </div>
    `}
    `
  }

  async obtainActualTemperature() {
    try {
      const url = `https://api.open-meteo.com/v1/forecast?latitude=LATITUD&longitude=LONGITUD&hourly=temperature_2m&current_weather=true&timezone=auto`;
      const position = await ActualUbicationUseCase.execute();
      const latitude = position.coords.latitude;
      const longitude = position.coords.longitude;

      const apiUrl = url.replace('LATITUD', latitude).replace('LONGITUD', longitude);

      const response = await axios.get(apiUrl);
      //console.log(response);
      const temperature = response.data.current_weather.temperature;
      this.temperature = temperature;
      //console.log(this.temperature)
    } catch (error) {
      console.log('Error:', error);
    }
  }

  recommendedRecipes(temperature){
    temperature = this.obtainActualTemperature();
    ObtainRecipeUseCase.execute(temperature)
      .then(recipes => {
        this.recipes = recipes;
      })
      .catch(error => {
        console.log(error);
      })
  }

  showRecipeDetails(recipe) {
    ShowRecipeUseCase.execute(recipe)
      .then(details => {
        this.selectedRecipe = details;
      })
      .catch(error => {
        console.log(error);
      });
  }

  createRenderRoot(){
    return this;
  }

} 
  customElements.define("home-page", HomePage);