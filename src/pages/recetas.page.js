import { LitElement, css, html } from "lit";
import { SearchRecipesUseCase } from "../usecases/search-recipes.usecase";
import { ShowRecipeUseCase } from "../usecases/show-recipe.usecase";



export class RecetasPage extends LitElement {

  static get styles (){
    return css`
      
    `;
  }

  static get properties(){
    return {
      recipes:{ type: Array },
      selectedRecipe: { type: Object},
    }
  }

  constructor() {
    super();
    this.recipes=[];
    this.selectedRecipe = null;
  }
  
  connectedCallback() {
    super.connectedCallback();

  }

  render(){
    return html `
    <div class="search-container">
      <input class="search-input" type="text" id="searchInput" placeholder="Buscar recetas...">
      <button class="search-button" id="searchButton" @click="${this.searchRecipes}">Buscar</button>
    </div>
    
    <div class="recipes-container">
        ${this.recipes.map(
          recipe => html`
            <div class="recipe-card">
              <h3 class="recipe-title">${recipe.title}</h3>
              <img
                class="recipe-image"
                src="${recipe.image}"
                alt="${recipe.title}"
                @click="${() => this.showRecipeDetails(recipe)}"
              />
              
              ${this.selectedRecipe && this.selectedRecipe.id === recipe.id
                ? html`
                    <div class="recipe-details">
                      <h4>Steps:</h4>
                      <ol>
                        ${this.selectedRecipe.steps.map(
                          step => html`
                            <li>${step}</li>
                          `
                        )}
                      </ol>
                    </div>
                  `
                : ''}
            </div>
          `
        )}
    </div>
    `;
  }

  searchRecipes(){
    //https://stackoverflow.com/questions/55101967/getelementbyid-from-within-shadow-dom
    //https://stackoverflow.com/questions/55461707/how-to-query-children-of-lit-element-shadow-root-using-tag-name
    const mySearch = document.querySelector('#searchInput').value;

    SearchRecipesUseCase.execute(mySearch)
      .then(recipes => {
        this.recipes = recipes;
      })
      .catch(error => {
        console.log(error);
      });
  }

  showRecipeDetails(recipe) {
    ShowRecipeUseCase.execute(recipe)
      .then(details => {
        this.selectedRecipe = details;
      })
      .catch(error => {
        console.log(error);
      });
  }

  createRenderRoot(){
    return this;
  }

}
  
customElements.define("recetas-page", RecetasPage);