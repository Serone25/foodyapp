import { LitElement, html, css } from 'lit';

import RecipeSearchByCaloriesUseCase from '../usecases/search-calories.usecase';
import { ShowRecipeUseCase } from "../usecases/show-recipe.usecase";

export class RecipeSearchByCaloriesComponent extends LitElement {
  static styles = css`
    
  `;

  static properties = {
    minCalories: { type: Number },
    maxCalories: { type: Number },
    recipes: { attribute: false },
    selectedRecipe: { type: Object},
  };

  constructor() {
    super();
    this.minCalories = 0;
    this.maxCalories = 0;
    this.recipes = [];
    this.caloriesUseCase = new RecipeSearchByCaloriesUseCase();
  }



  render() {
    return html`
        <div class="search-button-container">
            <div class="search-calories-container">
                <div class="input-container">
                    <label for="minCalories">Mínimo de calorías:</label>
                    <input id="minCalories" type="number" .value="${this.minCalories}" @change="${this.handleMinCaloriesChange}">
                </div>
                <div class="input-container">
                    <label for="maxCalories">Máximo de calorías:</label>
                    <input id="maxCalories" type="number" .value="${this.maxCalories}" @change="${this.handleMaxCaloriesChange}">
                </div>
            </div>
            <button class="search-button" id="searchButton" @click="${this.handleSearch}">Buscar</button>
        </div>
            <div class = recipes-container>
            ${this.recipes.map(
                recipe => html`
                <div class ="recipe-card">
                  <h3 class="recipe-title">${recipe.title}</h3>
                    <img
                        class="recipe-image"
                        src="${recipe.image}"
                        alt="${recipe.title}"
                        @click="${() => this.showRecipeDetails(recipe)}"
                    />
                    
                    ${this.selectedRecipe && this.selectedRecipe.id === recipe.id
                    ? html`
                        <div class="recipe-details">
                          <h4>Steps:</h4>
                          <ol>
                            ${this.selectedRecipe.steps.map(
                              step => html`
                                <li>${step}</li>
                              `
                            )}
                          </ol>
                        </div>
                    `: ''}
                </div>
                `
            )}
        </div>
    `;
  }
  handleMinCaloriesChange(event) {
    this.minCalories = parseInt(event.target.value);
  }

  handleMaxCaloriesChange(event) {
    this.maxCalories = parseInt(event.target.value);
  }

  async handleSearch() {
    try {
      this.recipes = await this.caloriesUseCase.execute(this.minCalories, this.maxCalories);
    } catch (error) {
      console.log('Error en la búsqueda de recetas por calorías:', error);
    }
  }

  showRecipeDetails(recipe) {
    ShowRecipeUseCase.execute(recipe)
      .then(details => {
        this.selectedRecipe = details;
      })
      .catch(error => {
        console.log(error);
      });
  }

  createRenderRoot(){
    return this;
  }
}

customElements.define('calorias-page', RecipeSearchByCaloriesComponent);