import {html, css, LitElement} from 'lit';
import { SearchRecipesUseCase } from "../usecases/search-recipes.usecase";
import { ShowRecipeUseCase } from "../usecases/show-recipe.usecase";

class MenuPage extends LitElement{
  static styles = css`

  `;

  static properties = {
    searchResults: { type: Array },
    menu: { type: Array },
  };

  constructor() {
    super();
    this.searchResults = [];
    this.menu = [];
  }


  render() {
    return html`
      <h1 class="menu-title">HAZ TU PROPIO MENU DE COMIDA</h1>

      <div class="search-container">
      <input class="search-input" type="text" id="searchInput" placeholder="Buscar recetas...">
      <button class="search-button" @click="${this.searchRecipes}">Buscar</button>
    </div>

      ${this.renderSearchResults()}
      ${this.renderMenu()}
    `;
  }

  async searchRecipes(){
    //https://stackoverflow.com/questions/55101967/getelementbyid-from-within-shadow-dom
    //https://stackoverflow.com/questions/55461707/how-to-query-children-of-lit-element-shadow-root-using-tag-name
    const mySearch = document.querySelector('#searchInput').value;

    try{
      const recipes = await SearchRecipesUseCase.execute(mySearch);
      this.searchResults = recipes;
    } catch(error){
      console.log(error);
    }
  }

  showRecipeDetails(recipe) {
    ShowRecipeUseCase.execute(recipe)
      .then(details => {
        this.selectedRecipe = details;
      })
      .catch(error => {
        console.log(error);
      });
  }

  addToMenu(recipe) {
    this.menu = [...this.menu, recipe];
  }

  removeFromMenu(recipeId) {
    this.menu = this.menu.filter(recipe => recipe.id !== recipeId);
  }

  clearMenu() {
    this.menu = [];
  }

  renderSearchResults() {
    return html`
      <h2 class="search-results">Resultados de búsqueda</h2>
      ${this.searchResults.length === 0
        ? html`
            <p class="search-results-p">No se encontraron resultados.</p>
          `
        : html`
            <div class="recipes-container">
              ${this.searchResults.map(
                recipe => html`
                  <div class="recipe-card">
                    <h3 class="recipe-title">${recipe.title}</h3>
                    <img
                      class="recipe-image"
                      src="${recipe.image}"
                      alt="${recipe.title}"
                      @click="${() => this.showRecipeDetails(recipe)}"
                    />
                    ${this.selectedRecipe && this.selectedRecipe.id === recipe.id
                      ? html`
                          <div class="recipe-details">
                            <h4>Steps:</h4>
                            <ol>
                              ${this.selectedRecipe.steps.map(
                                step => html`
                                  <li>${step}</li>
                                `
                              )}
                            </ol>
                          </div>
                        `
                      : ''}
                    <button class="add-delete-button" @click="${() => this.addToMenu(recipe)}">Agregar al menú</button>
                  </div>
              `)}
            </div>
          `}
    `;
  }

  renderMenu() {
    return html`
      <h2 class="menu">Menú de comida</h2>
      ${this.menu.length === 0
        ? html`
            <p class="menu-p">No hay recetas en el menú.</p>
          `
        : html`
            <div class="recipes-container" id="menu-recipes-container">
              ${this.menu.map(
                recipe => html`
                <div class="recipe-card">
                  <h3 class="recipe-title">${recipe.title}</h3>
                  <img
                    class="recipe-image"
                    src="${recipe.image}"
                    alt="${recipe.title}"
                    @click="${() => this.showRecipeDetails(recipe)}"
                  />
                  <button class="add-delete-button" @click="${() => this.removeFromMenu(recipe.id)}">Eliminar del menú</button>
                </div>
              `)}
            </div>
          `
      }
    `;
  }

  showMenuDetails() {
    console.log('Detalles del menú:', this.menu);
  }

  createRenderRoot(){
    return this;
  }
}

customElements.define("menu-page", MenuPage);