
export class NavBarElement extends HTMLElement{
    constructor(){
        super();
        console.log("navbar cargada");
    }

    connectedCallback(){
        this.innerHTML = `
            <style>
                .navbar {
                    display: flex;
                    justify-content: space-between;
                    height:80px;
                    background-color: #6FD513;
                }
                .navbar_link {
                    font-size: 24px;
                    margin-right: 20px;
                    margin-left:20px;
                }
            </style>
            <nav class = "navbar">
                <a class="link" href = "/">HOME</a>
                <a class="link" href = "/recetas">RECETAS</a>
                <a class="link" href = "/calorias">CALORIAS</a>
                <a class="link" href= "/menu">MENU</a>
            </nav>
        `;
    }
}

customElements.define("navbar-element", NavBarElement);