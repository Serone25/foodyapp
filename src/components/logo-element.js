export class LogoElement extends HTMLElement{
    constructor(){
        super();
        console.log("Logo cargado");
    }

    connectedCallback(){
        this.innerHTML = `
            <style>
                .logo_div {
                    height: 10vh;
                    border: 2px;
                    border-style: solid;
                    border-color: #6FD513;
                    margin-bottom:10px;
                    text-align:center;
                    color:white;
                    font-size: 80px;
                    font-weight:600;
                }
            </style>
            <div class = "logo_div">FOOD APP</div>
            `;
    }
}

customElements.define("logo-element", LogoElement);