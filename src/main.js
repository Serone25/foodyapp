import './main.css';
import './components/logo-element'
import './components/navbar-element'
import './pages/home.page';
import './pages/menu.page';
import './pages/recetas.page';
import './pages/calorias.page'
import { Router } from '@vaadin/router';

const outlet = document.getElementById('outlet');
const router = new Router(outlet);

router.setRoutes([
    { path: '/', component: 'home-page' },
    { path: '/recetas', component: 'recetas-page' },
    { path: '/menu', component: 'menu-page' },
    { path: '/calorias', component: 'calorias-page'},
    { path: '(.*)', redirect: '/'}
]);

console.log('Vanilla');
