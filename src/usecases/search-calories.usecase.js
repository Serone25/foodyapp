import {RecipeRepository} from '../repositories/recipe.repository';

class RecipeSearchByCaloriesUseCase {
  constructor() {
    this.repository = new RecipeRepository();
  }

  async execute(minCalories, maxCalories) {
    if (minCalories > maxCalories) {
      throw new Error('El rango de calorías es inválido');
    }
    const recipes = await this.repository.searchByCalories(minCalories, maxCalories);
    return recipes;
  }
}

export default RecipeSearchByCaloriesUseCase;