export class ShowRecipeUseCase {
    static async execute(recipe) {
      const apiKeySpoon = '3c679a55b9d8449eb6c35832d436b100';
      const apiUrl = `https://api.spoonacular.com/recipes/${recipe.id}/analyzedInstructions?apiKey=${apiKeySpoon}`;
  
      try {
        const response = await fetch(apiUrl);
        const data = await response.json();
  
        if (data.length > 0) {
          const steps = data[0].steps.map(step => step.step);
          return { ...recipe, steps };
        }
  
        return null;
      } catch (error) {
        throw new Error('Error al obtener los detalles de la receta');
      }
    }
  }