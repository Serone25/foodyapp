export class ActualUbicationUseCase {
    static async execute(){
        return new Promise((resolve, reject) => {
            if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(
                position => {
                    resolve(position);
                },
                error => {
                    reject(error);
                }
          );
        } else {
          reject(new Error('Geolocalización no soportada por el navegador.'));
        }
      });
    }
}