 export class SearchRecipesUseCase {
    static async execute(query) {
      const apiKeySpoon = '3c679a55b9d8449eb6c35832d436b100';
      const apiUrl = `https://api.spoonacular.com/recipes/search?apiKey=${apiKeySpoon}&query=${query}`;
  
      try {
        const response = await fetch(apiUrl);
        const data = await response.json();
  
        const recipes = data.results.map(recipe => ({
          id: recipe.id,
          title: recipe.title,
          image: `https://spoonacular.com/recipeImages/${recipe.id}-480x360.jpg`,
        }));
        console.log(recipes);
  
        return recipes;
      } catch (error) {
        throw new Error('Error al buscar recetas');
      }
    }
  }