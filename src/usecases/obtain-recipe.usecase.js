export class ObtainRecipeUseCase {
    static async execute(temperature) {
        const apiKeySpoonacular = '3c679a55b9d8449eb6c35832d436b100';
        let recomendedRecipe = "";
        //console.log(temperature);
        
        if(temperature <= 10){
          recomendedRecipe ="soup";
        }else if(temperature <= 20){
          recomendedRecipe ="stew";
        }else{
          recomendedRecipe="salad";
        }
        const urlSpoonacular = `https://api.spoonacular.com/recipes/complexSearch?apiKey=${apiKeySpoonacular}&query=${recomendedRecipe}`;
    
        const response = await fetch(urlSpoonacular);
        const data = await response.json();
        const recipes = data.results.map(recipe => ({
          id: recipe.id,
          title: recipe.title,
          image: `https://spoonacular.com/recipeImages/${recipe.id}-480x360.jpg`,
        }));
        //console.log(recipes);
    
        return recipes;
      }
}