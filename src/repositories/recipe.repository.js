export class RecipeRepository {
    constructor(apiKey) {
      this.apiKey = '3c679a55b9d8449eb6c35832d436b100'
    }
  
    async searchByCalories(minCalories, maxCalories) {
      const url = `https://api.spoonacular.com/recipes/complexSearch?apiKey=${this.apiKey}&minCalories=${minCalories}&maxCalories=${maxCalories}`;
  
      try {
        const response = await fetch(url);
        const data = await response.json();
        return data.results;
      } catch (error) {
        throw new Error('Error en la búsqueda de recetas:', error);
      }
    }
  }