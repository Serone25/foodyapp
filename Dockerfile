FROM node:18.0.0-alpine as build
RUN mkdir /ng-app
WORKDIR /ng-app

COPY package.json package-lock.json ./
RUN npm install
COPY . .
RUN npm run build -- --output-path=dist
#EXPOSE 3000

#CMD ["npm", "start"]

FROM nginx:1.13.3-alpine
COPY nginx/default.conf /etc/nginx/conf.d/
RUN rm -rf /usr/share/nginx/html/*
COPY --from=build /ng-app/dist /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]