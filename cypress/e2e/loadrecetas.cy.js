describe('template spec', () => {
  it('shows recipes succesfully', () => {
    cy.visit('http://localhost:8080/recetas');
    cy.get('.search-container').as('searchContainer')
    cy.get('@searchContainer').should('exist')
    cy.get('@searchContainer').find('.search-input').as('searchInput')
    cy.get('@searchInput').should('exist')
    cy.get('@searchInput').type('salad')
    cy.get('@searchContainer').find('.search-button').as('searchButton')
    cy.get('@searchButton').should('exist')
    cy.get('@searchButton').click()
    cy.get(':nth-child(1) > .recipe-title').as('recipeTitle')
    cy.get('@recipeTitle').should('exist')
    cy.get('@recipeTitle').should('have.text', 'Cannellini Bean and Asparagus Salad with Mushrooms')
  })
})