import { ShowRecipeUseCase } from "../src/usecases/show-recipe.usecase";

describe('ShowRecipeDetailsUseCase', () => {
    it('should return recipe details', async () => {
        const recipe = { id: 1, title: 'Chicken Recipe', image: 'chicken.jpg' };
        const expectedRecipe = await ShowRecipeUseCase.execute(recipe);
        expect(expectedRecipe.title).toEqual(recipe.title);
    });
});