import { ActualUbicationUseCase } from '../src/usecases/actual-ubication.usecase';

describe('ActualUbicationUseCase', () => {
  test('debería resolver la posición actual', async () => {
    //https://stackoverflow.com/questions/43008925/how-to-mock-navigator-geolocation-in-a-react-jest-test
    //https://www.npmjs.com/package/jest-environment-jsdom ----make tests in dom with jest
    //npm install --save-dev jest-environment-jsdom
    //{"jest": {"testEnvironment": "jest-environment-jsdom"}} en package.json

     const mockGeolocation = {
        getCurrentPosition: jest.fn()
          .mockImplementationOnce((success) => Promise.resolve(success({
            coords: {
              latitude: 51.1,
              longitude: 45.3
            }
          })))
      };
    global.navigator.geolocation = mockGeolocation;
    

    const position = await ActualUbicationUseCase.execute();
    await expect(position).toBeDefined();
  });
})