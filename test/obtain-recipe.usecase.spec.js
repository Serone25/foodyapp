import { ObtainRecipeUseCase } from '../src/usecases/obtain-recipe.usecase';

describe('ObtainRecipeUseCase', () => {
  beforeEach(() => {
    //API fetch simulated answer
    //https://jestjs.io/es-ES/docs/mock-function-api#mockfnmockimplementationfn
    //https://developero.io/blog/jest-mock-module-function-class-promises-axios-y-mas#c%C3%B3mo-hacer-mock-de-promises-o-promesas
    global.fetch = jest.fn().mockImplementation(() =>
      Promise.resolve({
        json: () =>
          Promise.resolve({
            results: [
              { id: 1, title: 'Recipe 1' },
              { id: 2, title: 'Recipe 2' },
              { id: 3, title: 'Recipe 3' },
            ],
          }),
      })
    );
  });

  test('it should be  a salad recipe', async () => {
    const temperature = 25;
    const recipes = await ObtainRecipeUseCase.execute(temperature);

    expect(recipes).toHaveLength(3);
    expect(recipes[0]).toEqual({
      id: 1,
      title: 'Recipe 1',
      image: 'https://spoonacular.com/recipeImages/1-480x360.jpg',
    });
  });


})