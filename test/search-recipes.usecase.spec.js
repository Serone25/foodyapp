import { SearchRecipesUseCase } from '../src/usecases/search-recipes.usecase';

describe('SearchRecipesUseCase', () => {
    it('should return recipes', async () => {
        const mySearch = 'chicken';
        const recipes = await SearchRecipesUseCase.execute(mySearch);

        const expectedRecipes = [
                {
                  id: 637876,
                  title: 'Chicken 65',
                  image: 'https://spoonacular.com/recipeImages/637876-480x360.jpg'
                },
                {
                  id: 629963,
                  title: 'chilli chicken',
                  image: 'https://spoonacular.com/recipeImages/629963-480x360.jpg'
                },
                {
                  id: 632810,
                  title: 'Asian Chicken',
                  image: 'https://spoonacular.com/recipeImages/632810-480x360.jpg'
                },
                {
                  id: 633959,
                  title: 'Balti Chicken',
                  image: 'https://spoonacular.com/recipeImages/633959-480x360.jpg'
                },
                {
                  id: 634476,
                  title: 'Bbq Chicken',
                  image: 'https://spoonacular.com/recipeImages/634476-480x360.jpg'
                },
                {
                  id: 634891,
                  title: 'Best Chicken Parmesan',
                  image: 'https://spoonacular.com/recipeImages/634891-480x360.jpg'
                },
                {
                  id: 636488,
                  title: 'Butter Chicken',
                  image: 'https://spoonacular.com/recipeImages/636488-480x360.jpg'
                },
                {
                  id: 637999,
                  title: 'Chicken Burritos',
                  image: 'https://spoonacular.com/recipeImages/637999-480x360.jpg'
                },
                {
                  id: 638002,
                  title: 'Chicken Cacciatore',
                  image: 'https://spoonacular.com/recipeImages/638002-480x360.jpg'
                },
                {
                  id: 638086,
                  title: 'Chicken Fingers',
                  image: 'https://spoonacular.com/recipeImages/638086-480x360.jpg'
                }
              ]
        expect(recipes).toEqual(expectedRecipes);
        expect(recipes[0].title).toBe(expectedRecipes[0].title);
        expect(recipes[1].title).toBe(expectedRecipes[1].title);
    });
});